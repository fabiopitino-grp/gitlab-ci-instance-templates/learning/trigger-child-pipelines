# Trigger child pipelines

Learn about Parent-Child pipelines.

```yaml
microservice_a:
  trigger:
    include: path/to/microservice_a.yml
    strategy: depend
```

You can include multiple files when defining a child pipeline. The child pipeline’s configuration is composed of all configuration files merged together:

```yaml
microservice_a:
  trigger:
    include:
      - local: path/to/microservice_a.yml
      - project: path/to/project
        file: .gitlab-ci.yml
        ref: master
    strategy: depend
```
